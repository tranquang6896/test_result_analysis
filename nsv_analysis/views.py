from django.shortcuts import render
from django.http import HttpResponse, JsonResponse
from django.conf import settings
from django.views.generic import View
from django.template.loader import get_template

import pandas as pd
import plotly
import plotly.express as px
import numpy as np
from sqlalchemy import create_engine
import matplotlib.pyplot as plt
import os
from xhtml2pdf import pisa
import json
from nsv_analysis import config_local
import matplotlib.dates as mdates

class GeneratePdf(View):
    def get(self, request, date, testid, filename, *args, **kwargs):
          engine = create_engine('mysql://' + config_local.MYSQL_USER + ':' + config_local.MYSQL_PASS + '@localhost/' + config_local.MYSQL_DB)

          # ******************START ANALYSIS***************************
          tb_team_sql = "select tblTTest.StaffID, tblTTest.Scale_100, tblMStaff3.Part, tblMStaff3.Team from tblTTest inner join tblMStaff3 on tblTTest.StaffID=tblMStaff3.StaffID WHERE tblTTest.TestID='" + testid + "' and tblTTest.TestDate='"+ date +"'"
          # create dataframe from SQL result
          data = pd.read_sql(tb_team_sql, engine)

          teams = set(data['Team'])
          teams = list(teams)
          teams.sort()

          S_dict , M_dict = {},{}
          tmp_team = teams[0][0:3]
          S_total ,M_total = 0,0

          for team in teams:
               if team[0:3] not in S_dict:
                    S_dict[team[0:3]] = {}
               if team[0:3] not in M_dict:
                    M_dict[team[0:3]] = {}

               if team[0:3] != tmp_team:
                    if 'Childs' in S_dict[tmp_team]:
                         S_dict[tmp_team]['TotalScore'] = round(S_total/len(S_dict[tmp_team]['Childs']),2)
                         S_total = 0
                    if 'Childs' in S_dict[tmp_team]:
                         M_dict[tmp_team]['TotalScore'] = round(M_total/len(M_dict[tmp_team]['Childs']),2)
                         M_total = 0

               S_data = data[(data['Team'].str.contains(team)) & (data['Part'] == 'Seisaku')]
               M_data = data[(data['Team'].str.contains(team)) & (data['Part'] == 'Madoguchi')]

               S_score = round(S_data['Scale_100'].sum() / len(S_data), 2)
               M_score = round(M_data['Scale_100'].sum() / len(M_data), 2)

               tmp_team = team[0:3]

               if np.isnan(S_score) == False:
                    if 'Childs' in S_dict[team[0:3]]:
                         S_dict[team[0:3]]['Childs'].append({'Subteam': team[3:4], 'Staff':len(S_data), 'Score':S_score})
                    else:
                         S_dict[team[0:3]]['Childs'] = [{'Subteam': team[3:4], 'Staff':len(S_data), 'Score':S_score}]

                    S_total += S_score

               if np.isnan(M_score) == False:
                    if 'Childs' in M_dict[team[0:3]]:
                         M_dict[team[0:3]]['Childs'].append({'Subteam': team[3:4], 'Staff':len(M_data), 'Score':M_score})
                    else:
                         M_dict[team[0:3]]['Childs'] = [{'Subteam': team[3:4], 'Staff':len(M_data), 'Score':M_score}]
                    
                    M_total += M_score
        
    
               if(team == teams[-1]):
                    if team[0:3] not in S_dict:
                         S_dict[team[0:3]] = {}
                    if team[0:3] not in M_dict:
                         M_dict[team[0:3]] = {}

                    if S_dict[team[0:3]] and 'Childs' in S_dict[team[0:3]]:
                         S_dict[team[0:3]]['TotalScore'] = round(S_total/len(S_dict[team[0:3]]['Childs']),2)
                    if M_dict[team[0:3]] and 'Childs' in M_dict[team[0:3]]:
                         M_dict[team[0:3]]['TotalScore'] = round(M_total/len(M_dict[team[0:3]]['Childs']),2)

          for team in S_dict:
               if 'Childs' in S_dict[team] and len(S_dict[team]['Childs']) == 1:
                    for x in S_dict[team]['Childs']:
                         x.pop('Score')
                         
          for team in M_dict:
               if 'Childs' in M_dict[team] and len(M_dict[team]['Childs']) == 1:
                    for x in M_dict[team]['Childs']:
                         x.pop('Score')
          S_json = json.dumps(S_dict)
          S_data = json.loads(S_json)
          M_json = json.dumps(M_dict)
          M_data = json.loads(M_json)
          #***************************END ANALYSIS********************************

          #*****************************START PLOT*******************************
          query = "SELECT tblTTest.StaffID, tblTTest.Scale_100 as Score, tblMStaff3.Part, tblMStaff.TrialEntryDate as EntryDate FROM tblTTest INNER JOIN tblMStaff3 ON tblTTest.StaffID=tblMStaff3.StaffID INNER JOIN tblMStaff ON tblMStaff.StaffID=tblTTest.StaffID where tblTTest.TestID='TN001' and tblTTest.TestDate='2020-04-28'"
          df = pd.read_sql(query, engine)
          df = pd.DataFrame(data=df)

          df_S = df[df['Part'] == 'Seisaku']
          df_M =df[df['Part'] == 'Madoguchi']

          # ******************** IMAGE PLOT
          fig2, ax = plt.subplots(figsize=(11, 4))
          ax.plot(df_S['EntryDate'], df_S['Score'], 'ro', label='Seisaku')
          ax.plot(df_M['EntryDate'], df_M['Score'], 'go', label='Madoguchi')

          # Set title and labels for axes
          ax.set(xlabel="EntryDate(Year)",
               ylabel="Score(Scale-100)",
               )

          # Define the date format
          date_form = mdates.DateFormatter("%Y")
          ax.xaxis.set_major_formatter(date_form)

          # Ensure a major tick for each week using (interval=1) 
          ax.xaxis.set_major_locator(mdates.YearLocator())
          plt.legend(loc='best')
          plt.yticks(np.arange(0, 110, 10.0))
          file_img = 'files/export/' + filename + '.png';
          plt.savefig(file_img, bbox_inches='tight')
          

          # ******************** HTML PLOT
          fig = px.scatter(   df, 
                              x="EntryDate", 
                              y="Score", 
                              color="Part",
                              title="Test Result Analysis",
                              hover_data=['StaffID'],
                              # labels={"salary":"Annual Salary (in thousands)"} # customize axis label
                         )
          fig.update_layout(
               xaxis_tickformat = '%m / %d<br>%Y',
               width=1000,
               height=400,
          )

          plot = plotly.offline.plot(fig,
                            config={"displayModeBar": False},
                            show_link=False,
                            include_plotlyjs=False,
                            output_type='div')
          file_html = 'files/export/' + filename + '.html';
          fig.write_html(file_html) 
          #              
          #****************************END PLOT**********************************
          
          template = get_template('analysis.html')
          html = template.render({'link_img':file_img, 'link_html':file_html, 'host':config_local.HOST, 'S_dict':S_data, 'M_dict':M_data})
          file = open('files/{}.pdf'.format(filename), "w+b")
          pisaStatus = pisa.CreatePDF(html.encode('utf-8'), dest=file, encoding='utf-8')
          file.seek(0)
          pdf = file.read()
          file.close() 
          return HttpResponse(pdf, 'application/pdf')