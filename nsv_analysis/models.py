from django.db import connections
from django.db import models

class TBLStaff(models.Model):
     StaffID = models.CharField(primary_key=True),
     StaffName = models.CharField(),
     JapaneseName = models.CharField(),
     Birthday = models.DateField(),
     TrialEntryDate = models.DateField(),
     TrialEndDate = models.DateField(),
     Password = models.CharField(),
     QuitJobDate = models.DateField(),
     DateUpdate = models.DateTimeField(),
     class Meta:
          db_table = "tblMStaff"

class TBLStaff2(models.Model):
     StaffID = models.CharField(primary_key=True),
     Email = models.CharField(),
     PhoneNumber = models.CharField(),
     Gender = models.CharField(),
     MarialStatus = models.IntegerField(),
     class Meta:
          db_table = "tblMStaff2"

class TBLStaff3(models.Model):
     StaffID = models.CharField(primary_key=True),
     FromTeam = models.CharField(),
     Team = models.CharField(),
     Position = models.CharField(),
     Part = models.CharField(),
     Year = models.CharField(),
     class Meta:
          db_table = "tblMStaff3"

class TBLMTest(models.Model):
     ID = models.IntegerField(primary_key=True),
     TestID = models.CharField(),
     TypeOfStaff = models.CharField(),
     Description = models.CharField(),
     TestDate = models.CharField(),
     class Meta:
          db_table = "tblMTest"

class TBLTTest(models.Model):
     ID = models.IntegerField(primary_key=True),
     TestID = models.CharField(),
     Description = models.CharField(),
     TypeOfStaff = models.CharField(),
     StaffID = models.CharField(),
     MaxScore = models.IntegerField(),
     TotalScore = models.IntegerField(),
     Scale_100 = models.FloatField(),
     Ranks = models.IntegerField(),
     class Meta: 
          db_table = "tblTTest"


